#ifndef INP_H_
#define INP_H_

extern WORD g_inp, g_inp_down, g_inp_up;
extern WORD g_inp_rep, g_inp_rep_down, g_inp_rep_up;

void inp_init(void);
void inp_finish(void);

void inp_rep_clear(void);

void inp_update(void);

void inp_set_repeat_rate(int rate);
void inp_set_repeat_delay(int delay);

#endif
