#ifndef VGM_H_
#define VGM_H_

#define VGM_VOLUME_MAX 0x100
#define VGM_VOLUME_STEP 0x10

typedef struct {
  SWORD min, sec, dframe;
  WORD valid;
} TimeStamp;

void vgm_init(void);

int vgm_load(BYTE far *file, DWORD size);
void vgm_start(void);
void vgm_pause(void);
void vgm_stop(void);

int vgm_is_vgz(void);

void vgm_set_loop_count(int count);
int vgm_loop_count(void);

void vgm_volume_set(SWORD level);
WORD vgm_volume_level(void);
void vgm_volume_down(void);
void vgm_volume_up(void);

int vgm_is_over(void);
int vgm_is_playing(void);

TimeStamp vgm_time_intro(void);
TimeStamp vgm_time_loop(void);
TimeStamp vgm_time_full(void);

#endif
