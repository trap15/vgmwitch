#ifndef TOP_H_
#define TOP_H_

#define DEFLATE_SUPPORT 1

#define DEBUG 0
#define XY_SWAP 0

#ifdef LSI_C
int _asm_inline(char near *asm);
#define _CS _asm_inline("\tmov\tax,cs");
#define _DS _asm_inline("\tmov\tax,ds");
#endif

#if XY_SWAP
#define SCR_W 144
#define SCR_H 224
#else
#define SCR_W 224
#define SCR_H 144
#endif

#include <machine.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/bios.h>

typedef signed char  SBYTE;
typedef signed short SWORD;
typedef signed long  SDWORD;

void sound_mute(void);
void sound_unmute(void);

#endif
