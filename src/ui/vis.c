#include "top.h"

#define VIS_XPOS 19
#define VIS_YPOS 1

static BYTE s_volumes[4];

static void prv_vis_poses(int ch, int far *xpos, int far *ypos)
{
  *xpos = VIS_XPOS;
  *ypos = VIS_YPOS;
  *xpos += ch * 2;
}

static void prv_vis_chname(int ch)
{
  int xpos, ypos;
  prv_vis_poses(ch, &xpos, &ypos);
  text_put_char(xpos+0,ypos+0,'C');
  text_put_char(xpos+1,ypos+0,'1'+ch);
}

void vis_setup(void)
{
  int i;
  for(i = 0; i < 4; i++)
    prv_vis_chname(i);
}

#define VIS_VOLHEIGHT 9
static void prv_vis_vol_side(int ch, int is_right, int lvl)
{
  int xpos,ypos, i, c;
  lvl = lvl * VIS_VOLHEIGHT / 15;
  prv_vis_poses(ch, &xpos, &ypos);
  ypos += 1;
  xpos += is_right ? 1 : 0;
  for(i = 0; i < VIS_VOLHEIGHT+1; i++) {
    if(i < lvl) c = '|';
    else if(i == lvl) c = 0x1F;
    else c = ' ';
    text_put_char(xpos,ypos+i, c);
  }
}

static void prv_vis_vol(int ch, BYTE vol)
{
  prv_vis_vol_side(ch, 0, (vol >> 4) & 0xF);
  prv_vis_vol_side(ch, 1, (vol >> 0) & 0xF);
}

void vis_update(void)
{
  int i;
  for(i = 0; i < 4; i++) {
    prv_vis_vol(i, s_volumes[i]);
  }
}

void vis_set_volume(int ch, BYTE l, BYTE r)
{
  s_volumes[ch] = (l << 4) | (r << 0);
}
