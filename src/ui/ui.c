#include "top.h"

#include "inp.h"
#include "gfx.h"
#include "vgm.h"
#include "ui/ui.h"
#include "ui/vis.h"
#include "ui/gd3.h"

#define FILEMAX 128

#define UIFLAG_AUTOADVANCE (1 << 0)
#define UIFLAG_REPEAT (1 << 1)

#define REDISP_LIST (1 << 0)
#define REDISP_GD3 (1 << 1)
#define REDISP_VIS (1 << 2)
#define REDISP_TIME (1 << 3)
#define REDISP_VOLUME (1 << 4)

#define REDISP_ALL (0xFF)

typedef struct {
  /* Config */
  WORD flag;
  BYTE redisp;

  /* UI state */
  SWORD scroll_pos;
  SWORD cursor_pos;
  SWORD playing_pos;

  TimeStamp time_cur, time_full;
  TimeStamp time_intro, time_loop;

  /* File list */
  char fnames[FILEMAX][MAXFNAME+1];
  WORD entries;
} UiState;

static UiState s_ui;

static void prv_get_file_list(UiState *ui)
{
  int dir_entries;
  int i, l;
  struct stat statbuf;
  BYTE far *file;

  text_put_string((28-7)/2, 9, "LOADING");
  dir_entries = nument("/rom0/");

  ui->entries = 0;
  text_put_numeric(14+1,11, 4, NUM_PADSPACE | NUM_ALIGN_LEFT, dir_entries);
  text_put_char(14,11, '/');
  for(i = 0; i < dir_entries; i++) {
    text_put_numeric(14-4,11, 4, NUM_PADSPACE | NUM_ALIGN_RIGHT, i);
    if(getent("/rom0/", i, &statbuf) != E_FS_SUCCESS) {
      continue;
    }

    file = mmap(statbuf.name);
    if(strncmp(file, "Vgm ", 4) != 0) { /* Check for VGM header */
#if DEFLATE_SUPPORT
      if(strncmp(file, "\x1F\x8B\x08", 3) != 0) { /* Check for DEFLATE header */
        continue;
      }
#else
      continue;
#endif
    }

    /* De-duping */
    for(l = 0; l < ui->entries; l++) {
      if(strcmp(ui->fnames[l], statbuf.name) == 0) {
        break;
      }
    }
    if(l != ui->entries) { /* Was a duplicate */
      continue;
    }

    strncpy(ui->fnames[ui->entries], statbuf.name, MAXFNAME);
    ui->fnames[ui->entries][MAXFNAME] = '\0';
    ui->entries++;
  }
}

#define LIST_XPOS 0
#define LIST_YPOS 1
#define LIST_WIDTH 18
#define LIST_HEIGHT 10

#define LISTCURSOR_XPOS 0
#define LISTPLAY_XPOS 1
#define LISTFILE_XPOS 2
#define LISTFILE_COUNT LIST_HEIGHT

static void prv_show_one_file(int yoff, const char *name)
{
  text_put_string(LISTFILE_XPOS, LIST_YPOS+yoff, (const char far*)name);
}

static void prv_show_list(UiState *ui, int count)
{
  int i;
  screen_fill_char(SCREENTEXT, LIST_XPOS,LIST_YPOS-1,LIST_WIDTH,LIST_HEIGHT+2, 0x0800);
  if(ui->entries == 0) {
    text_put_string((28-12)/2, 9, "NO VGM FILES");
    return;
  }

  if(ui->entries - ui->scroll_pos < count)
    count = ui->entries - ui->scroll_pos;
  for(i = 0; i < count; i++) {
    prv_show_one_file(i, ui->fnames[ui->scroll_pos+i]);
  }
  i = ui->cursor_pos - ui->scroll_pos;
  if(i >= 0 && i < count)
    text_put_char(LISTCURSOR_XPOS, LIST_YPOS+i, 0x10);
  i = ui->playing_pos - ui->scroll_pos;
  if(i >= 0 && i < count)
    text_put_char(LISTPLAY_XPOS, LIST_YPOS+i, 0x0E);

  if(ui->scroll_pos != 0) {
    text_fill_char(LISTFILE_XPOS, LIST_YPOS-1, 3, 0x1E);
  }
  if(ui->scroll_pos != (ui->entries - LISTFILE_COUNT) && ui->entries > LISTFILE_COUNT) {
    text_fill_char(LISTFILE_XPOS, LIST_YPOS+LIST_HEIGHT, 3, 0x1F);
  }
}

void ui_init(void)
{
  UiState *ui = &s_ui;
  memset(ui, 0, sizeof(UiState));
  ui->flag |= UIFLAG_AUTOADVANCE | UIFLAG_REPEAT;
  prv_get_file_list(ui);
  ui->playing_pos = -1;
  ui->redisp = REDISP_ALL;

  gd3_load(NULL);
  vgm_init();

  text_put_string(28-4,0, "v0.3");
}

static void prv_scroll_down(UiState *ui)
{
  ui->cursor_pos++;
  if(ui->cursor_pos - ui->scroll_pos >= LISTFILE_COUNT)
    ui->scroll_pos++;
}

static void prv_scroll_up(UiState *ui)
{
  ui->cursor_pos--;
  if(ui->scroll_pos > ui->cursor_pos)
    ui->scroll_pos--;
}

static void prv_start(UiState *ui)
{
  BYTE far *vgmdata;
  int xpos,ypos;
  struct stat st;
  int vgmerr;

  ui->redisp |= REDISP_ALL;

  gd3_load(NULL);
  gd3_display();

  xpos = (28-7)/2;
  ypos = 14;
  text_put_string(xpos,ypos, "LOADING");

  stat(ui->fnames[ui->playing_pos], &st);
  vgmdata = mmap(ui->fnames[ui->playing_pos]);
  vgmerr = vgm_load(vgmdata, st.len);
  if(!vgmerr) {
    ui->time_cur.min = 0;
    ui->time_cur.sec = 0;
    ui->time_cur.dframe = 0;
    ui->time_cur.valid = 0;
    ui->time_intro = vgm_time_intro();
    ui->time_loop = vgm_time_loop();
    ui->time_full = vgm_time_full();

    vgm_start();

    text_fill_char(xpos,ypos,7, ' ');
  }else{
    ui->playing_pos = -1;
    text_put_string(xpos,ypos, "FAILED!");
    text_put_numeric(xpos,ypos+1, 4, NUM_HEXA, vgmerr);
    ui->redisp &= ~REDISP_GD3;
  }
}

static void prv_stop_core(UiState *ui)
{
  gd3_load(NULL);
  vgm_stop();
}

static void prv_stop(UiState *ui)
{
  prv_stop_core(ui);
  ui->playing_pos = -1;
  ui->redisp |= REDISP_ALL;
}

static void prv_next(UiState *ui)
{
  prv_stop_core(ui);
  if(ui->playing_pos == -1)
    return;
  ui->playing_pos++;
  if(ui->playing_pos >= ui->entries) {
    if(ui->flag & UIFLAG_REPEAT)
      ui->playing_pos -= ui->entries;
    else
      ui->playing_pos = -1;
  }
  if(ui->playing_pos != -1) {
    prv_start(ui);
  }
}

static void prv_prev(UiState *ui)
{
  prv_stop_core(ui);
  if(ui->playing_pos == -1)
    return;
  ui->playing_pos--;
  if(ui->playing_pos < 0) {
    if(ui->flag & UIFLAG_REPEAT)
      ui->playing_pos += ui->entries;
    else
      ui->playing_pos = -1;
  }
  if(ui->playing_pos != -1) {
    prv_start(ui);
  }
}

static void prv_put_timestamp(int xpos, int ypos, TimeStamp *stmp)
{
  text_put_numeric(xpos+0,ypos, 2, NUM_PADSPACE|NUM_ALIGN_RIGHT, stmp->min);
  text_put_char   (xpos+2,ypos,':');
  text_put_numeric(xpos+3,ypos, 2, NUM_PADZERO|NUM_ALIGN_RIGHT, stmp->sec);
}
static void prv_show_time(UiState *ui, int do_it)
{
  int show_intro;
  int show_loop;
  int show_mul;
  int width = 0;
  int xpos, ypos;

  ypos = 17;

  show_intro = ui->time_intro.valid;
  show_loop = ui->time_loop.valid;
  show_mul = vgm_loop_count() >= 2;

  width = 5+2+5+2+5+1+5+1+1;
  xpos = 28 - width;

  text_fill_char(xpos,ypos,width, ' ');
  if(!do_it)
    return;

  width = 0;

  width += 5; /* Time Cur */
  width += 2; /* ' /' */
  width += 5; /* Time Full */
  if(show_intro || show_loop) {
    width += 2; /* '(' and ')' */
  }
  if(show_intro) {
    width += 5; /* Time Intro */
    if(show_loop) {
      width += 1; /* '+' */
    }
  }
  if(show_loop) {
    width += 5; /* Time Loop */
    if(show_mul) {
      width += 1; /* 'x' */
      width += 1; /* mul */
    }
  }

  xpos = 28 - width;

  prv_put_timestamp(xpos,ypos, &ui->time_cur);
  xpos += 5;
  xpos += 1;
  text_put_char(xpos,ypos,'/');
  xpos += 1;
  prv_put_timestamp(xpos,ypos, &ui->time_full);
  xpos += 5;
  if(show_intro || show_loop) {
    text_put_char(xpos,ypos,'(');
    xpos += 1;
  }
  if(show_intro) {
    prv_put_timestamp(xpos,ypos, &ui->time_intro);
    xpos += 5;
    if(show_loop) {
      text_put_char(xpos,ypos, '+');
      xpos += 1;
    }
  }
  if(show_loop) {
    prv_put_timestamp(xpos,ypos, &ui->time_loop);
    xpos += 5;
    if(show_mul) {
      text_put_char(xpos,ypos, 'x');
      xpos += 1;
      text_put_numeric(xpos,ypos, 1, NUM_PADSPACE|NUM_ALIGN_LEFT, vgm_loop_count());
      xpos += 1;
    }
  }
  if(show_intro || show_loop) {
    text_put_char(xpos,ypos,')');
    xpos += 1;
  }
}

void ui_update(void)
{
  UiState *ui = &s_ui;
  /* Menu navigation */
  if(g_inp_rep_down & KEY_X1) {
    if(ui->cursor_pos > 0) {
      prv_scroll_up(ui);
      ui->redisp |= REDISP_LIST;
    }
  }
  if(g_inp_rep_down & KEY_X3) {
    if(ui->cursor_pos < ui->entries-1) {
      prv_scroll_down(ui);
      ui->redisp |= REDISP_LIST;
    }
  }
  /* Volume control */
  if(g_inp_rep_down & KEY_Y1) {
    vgm_volume_up();
    ui->redisp |= REDISP_VOLUME;
  }
  if(g_inp_rep_down & KEY_Y3) {
    vgm_volume_down();
    ui->redisp |= REDISP_VOLUME;
  }
  /* Play control */
  if(g_inp_down & KEY_Y2) {
    prv_next(ui);
  }
  if(g_inp_down & KEY_Y4) {
    prv_prev(ui);
  }
  if(g_inp_down & KEY_A) {
    ui->playing_pos = ui->cursor_pos;
    prv_start(ui);
  }
  if(g_inp_down & KEY_B) {
    vgm_pause();
  }
  if(g_inp_down & KEY_START) {
    /* TODO: Configure menu */
  }

  if(vgm_is_over() && ui->playing_pos != -1) {
    prv_stop_core(ui);
    ui->redisp |= REDISP_ALL;
    if(ui->flag & UIFLAG_AUTOADVANCE) {
      prv_next(ui);
    }else{
      ui->playing_pos = -1;
    }
  }

  if(!vgm_is_over()) {
    if(vgm_is_playing()) {
      ui->time_cur.dframe += 2;
      if(ui->time_cur.dframe >= 151) {
        ui->time_cur.dframe -= 151;
        ui->redisp |= REDISP_TIME;
        ui->time_cur.sec++;
        if(ui->time_cur.sec == 60) {
          ui->time_cur.sec -= 60;
          ui->time_cur.min++;
        }
      }
    }else if((g_frame & 15) == 0) {
      ui->redisp |= REDISP_TIME;
    }
  }

  if(ui->redisp & REDISP_VIS) {
    vis_setup();
    ui->redisp &= ~REDISP_VIS;
  }
  if(ui->redisp & REDISP_GD3) {
    gd3_display();
    ui->redisp &= ~REDISP_GD3;
  }
  if(ui->redisp & REDISP_TIME) {
    if(vgm_is_playing()) {
      prv_show_time(ui, 1);
    }else if(!vgm_is_over()) {
      prv_show_time(ui, g_frame & 16);
    }
    ui->redisp &= ~REDISP_TIME;
  }
  if(ui->redisp & REDISP_LIST) {
    prv_show_list(ui, LISTFILE_COUNT);
    ui->redisp &= ~REDISP_LIST;
  }
#define VOLUME_XPOS (28-9)
#define VOLUME_YPOS (0)
  if(ui->redisp & REDISP_VOLUME) {
    text_fill_char(VOLUME_XPOS,VOLUME_YPOS,4, ' ');
    text_put_numeric(VOLUME_XPOS,VOLUME_YPOS,3, NUM_PADSPACE|NUM_ALIGN_RIGHT, vgm_volume_level() * 100 / VGM_VOLUME_MAX);
    text_put_char(VOLUME_XPOS+3,VOLUME_YPOS,'%');
    ui->redisp &= ~REDISP_VOLUME;
  }
  vis_update();
}
