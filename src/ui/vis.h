#ifndef UI_VIS_H_
#define UI_VIS_H_

void vis_setup(void);
void vis_update(void);

void vis_set_volume(int ch, BYTE l, BYTE r);

#endif
