#include "top.h"

#include "gfx.h"

#define GD3_XPOS 0
#define GD3_YPOS 12
#define GD3_WIDTH 28
#define GD3_HEIGHT 5

#define GD3_STRLEN 28

typedef struct {
  int valid;
  char trackname[GD3_STRLEN+1];
  char gamename[GD3_STRLEN+1];
  char sysname[GD3_STRLEN+1];
  char artist[GD3_STRLEN+1];
  char release[GD3_STRLEN+1];
} Gd3State;

static Gd3State s_gd3;

static int prv_load_string(BYTE far *data, char *dest)
{
  /* Note this is really hacky and bad, but fuck UTF16 */
  WORD far *utf16 = (WORD far*)data;
  int idx = 0;
  int count = 0;
  while(*utf16 != 0) {
    if(idx < GD3_STRLEN && dest != NULL) {
      dest[idx++] = *utf16;
    }
    utf16++;
    count++;
  }
  count++;
  if(dest != NULL)
    dest[idx] = '\0';
  return count * 2;
}

void gd3_load(BYTE far *file)
{
  BYTE far *data;
  memset(&s_gd3, 0, sizeof(s_gd3));
  if(file == NULL) {
    return;
  }
  if(*(DWORD far*)(&file[0x14]) == 0) {
    return;
  }
  data = file + *(DWORD far*)(&file[0x14]) + 0x14;
  data += 0x0C;
  data += prv_load_string(data, s_gd3.trackname); /* English trackname */
  data += prv_load_string(data, NULL); /* Japanese trackname */
  data += prv_load_string(data, s_gd3.gamename); /* English gamename */
  data += prv_load_string(data, NULL); /* Japanese gamename */
  data += prv_load_string(data, s_gd3.sysname); /* English sysname */
  data += prv_load_string(data, NULL); /* Japanese sysname */
  data += prv_load_string(data, s_gd3.artist); /* English artist */
  data += prv_load_string(data, NULL); /* Japanese artist */
  data += prv_load_string(data, s_gd3.release); /* Release */
  s_gd3.valid = 1;
}

static void prv_display_one(int line, const char *str)
{
  int xpos,ypos;
  xpos = GD3_XPOS;
  ypos = GD3_YPOS + line;
  text_put_string(xpos, ypos, str);
}

void gd3_display(void)
{
  screen_fill_char(SCREENTEXT, GD3_XPOS,GD3_YPOS,GD3_WIDTH,GD3_HEIGHT, 0x0800);
  if(!s_gd3.valid)
    return;
  prv_display_one(0, s_gd3.trackname);
  prv_display_one(1, s_gd3.gamename);
  prv_display_one(2, s_gd3.artist);
  prv_display_one(3, s_gd3.release);
  prv_display_one(4, s_gd3.sysname);
}
