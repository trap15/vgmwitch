#include "top.h"

#include "gfx.h"

static intvector_t g_vblk_int;

typedef struct {
  WORD attr;
  BYTE y;
  BYTE x;
} RawSprite;

int g_rawspr_cnt;
static RawSprite g_rawsprs[HW_SPR_COUNT];
static Sprite g_sprs[SW_SPR_COUNT];
int g_frame;
static int g_delay_frame;
static BYTE g_dispctrl;

void gx_set_bgcolor(BYTE col)
{
  display_control(g_dispctrl | (col << 8));
}

void far gx_vblank(void)
{
  g_delay_frame++;
}

void gx_init(void)
{
  int i;
  intvector_t v;

  g_frame = 0;
  g_delay_frame = 0;

  screen2_set_window(0xF0,0xF0,0xF0,0xF0);
  g_dispctrl = DCM_SCR1 | DCM_SCR2 | DCM_SPR | DCM_SCR2_WIN_OUTSIDE;

	text_screen_init();
  text_set_screen(SCREENTEXT);
  screen_fill_char(SCREEN1, 0,0,32,32, 0x0800);
  screen_fill_char(SCREEN2, 0,0,32,32, 0x0800);
  text_set_palette(BGPAL_TEXT);
  font_set_color((0<<2) | (3<<0));

  gx_set_bgcolor(0);
  for(i = 0; i < 16; i++) {
    palette_set_color(i, 0x7420);
  }
  g_rawspr_cnt = 0;
  memset(g_sprs, 0, sizeof(g_sprs));

  palette_set_color(BGPAL_TEXT, 0x7530);

  v.callback = (void (near *)())FP_OFF(gx_vblank);
  v.cs = _CS;
  v.ds = _DS;
	sys_interrupt_set_hook(SYS_INT_VBLANK, &v, &g_vblk_int);
}
void gx_finish(void)
{
  sys_interrupt_reset_hook(SYS_INT_VBLANK, &g_vblk_int);
}

#define SPR_RENDER_CORE \
        if((spr->extra & 3) != 1) \
          continue; \
        raw->attr = spr->attr | spr->tile; \
        raw->x = (spr->x >> 8) - 4; \
        raw->y = (spr->y >> 8) - 4; \
        raw++; \
        if(++pos >= HW_SPR_COUNT) \
          goto nomorehw;

void gx_frame(void)
{
  int i;
  Sprite *spr;
  RawSprite *raw;
  int pos = 0;
  raw = g_rawsprs;
  if(g_frame & 1) {
    spr = g_sprs + SW_SPR_COUNT;
    for(i = 0; i < SW_SPR_COUNT; i++) {
      spr--;
      SPR_RENDER_CORE
    }
  }else{
    spr = g_sprs-1;
    for(i = 0; i < SW_SPR_COUNT; i++) {
      spr++;
      SPR_RENDER_CORE
    }
  }
nomorehw:
  g_rawspr_cnt = pos;

  if(g_delay_frame < 1)
    sys_wait(1);
  g_delay_frame = 0;
  g_frame++;

  sprite_set_range(0, g_rawspr_cnt);
  sprite_set_data(0, g_rawspr_cnt, g_rawsprs);
}
 
Sprite *gx_spr_alloc(void)
{
  int i;
  for(i = 0; i < SW_SPR_COUNT; i++) {
    if((g_sprs[i].extra & 1) == 0)
      return &g_sprs[i];
  }
  return NULL;
}

int gx_spr_alloc_count(int count, Sprite *sprs[])
{
  int i;
  for(i = 0; i < count; i++) {
    sprs[i] = gx_spr_alloc();
    if(sprs[i] == NULL) {
      for(i--; i >= 0; i--) {
        sprs[i]->extra = 0;
      }
      return 0;
    }
    sprs[i]->extra = 1;
    sprs[i]->y = 0xF000;
  }
  return 1;
}
