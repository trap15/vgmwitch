/* This file has been significantly modified for VGM Witch.
 * Rough outline of the changes:
 *  - Support for compiling on 16-bit x86 (LCC86 compiler)
 *  - Failed attempt at adding streaming support
 *  - Reduce stack usage
 *  - Hacked out setjmp/longjmp error reporting
 */

/* puff.h
  Copyright (C) 2002-2013 Mark Adler, all rights reserved
  version 2.3, 21 Jan 2013

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the author be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Mark Adler    madler@alumni.caltech.edu
 */

#include "top.h"

/* Currently broken */
#define STREAM_SUPPORT 0

#ifndef NULL
#if defined(_D_FAR)
#define NULL  (0L)
#else
#define NULL  (0)
#endif
#endif

#define PTR far*

#ifndef NIL
#  define NIL NULL
#endif

typedef unsigned char pf_uint8;
typedef unsigned short pf_uint16;
typedef unsigned long pf_uint32;
typedef signed char pf_int8;
typedef signed short pf_int16;
typedef signed long pf_int32;

typedef pf_uint8 PTR pf_uint8p;
typedef pf_uint16 PTR pf_uint16p;
typedef pf_uint32 PTR pf_uint32p;
typedef pf_int8 PTR pf_int8p;
typedef pf_int16 PTR pf_int16p;
typedef pf_int32 PTR pf_int32p;

#define PUFF_WORK_SIZE (24577+16384)

typedef struct {
    /* output state */
    pf_uint8p out;         /* output buffer */
    pf_uint32 outlen;       /* available space at out */
    pf_uint32 outcnt;       /* bytes written to out so far */
#if STREAM_SUPPORT
    pf_uint8 work[PUFF_WORK_SIZE];
    pf_uint32 work_idx;
#endif

    /* input state */
    pf_uint8p in;    /* input buffer */
    pf_uint32 inlen;        /* available input at in */
    pf_uint32 incnt;        /* bytes read so far */
    pf_int32 bitbuf;                 /* bit buffer */
    pf_int32 bitcnt;                 /* number of bits in bit buffer */

    int puff_failed;
} PuffState;

void puff_init(PuffState PTR stt,
               pf_uint8p source, pf_uint32 sourcelen);

pf_int32 puff(PuffState PTR stt,
              pf_uint8p dest, pf_uint32p destlen);
