#include "top.h"

#include "gfx.h"
#include "inp.h"
#include "vgm.h"
#include "ui/ui.h"

int g_running;

/* I dunno where else to put these */
void sound_mute(void)
{
  sound_set_output(0);
}
void sound_unmute(void)
{
  sound_set_output(0x9|(3<<1));
}

void main(int argc, char *argv[])
{
  inp_init();

restart:
  gx_init();
  ui_init();

  g_running = 1;
  while(g_running) {
    gx_frame();
    inp_update();
    if((g_inp & (KEY_A|KEY_START)) == (KEY_A|KEY_START)) {
      g_running = 0;
      break;
    }
    ui_update();
  }

  vgm_stop();
  inp_finish();
  gx_finish();
  bios_exit();
}
