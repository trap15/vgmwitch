#ifndef CHIP_SN76489_H_
#define CHIP_SN76489_H_

void sn76489_init(void);
void sn76489_write(BYTE data);
void sn76489_ggstereo(BYTE data);
void sn76489_set_volume(WORD vol);

#endif
