#include "top.h"

#include "vgm.h"
#include "ui/vis.h"
#include "chip/sn76489.h"

#define ANALOG_ARTIFACTING 0

typedef struct {
  int reg_latch;
  WORD regs[8];
  BYTE stereo;
  WORD vol;
} SN76489State;

static SN76489State s_sn76489;

#define LEVEL_COUNT 0x10

#define WAVE_LVL(lvl, v) (((v) * (lvl) + (LEVEL_COUNT-2)) / (LEVEL_COUNT-1))
#define WAVE_BYTE(lvl, v1,v2) WAVE_LVL(lvl, v1) | (WAVE_LVL(lvl, v2) << 4)
#define MAIN_WAVE_ANL(lvl) { \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xE,0xE), \
  WAVE_BYTE(lvl,0xE,0xD), \
  WAVE_BYTE(lvl,0xD,0xD), \
  WAVE_BYTE(lvl,0xD,0xD), \
  WAVE_BYTE(lvl,0xC,0xC), \
  WAVE_BYTE(lvl,0xC,0xC), \
  WAVE_BYTE(lvl,0xC,0xC), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x1,0x1), \
  WAVE_BYTE(lvl,0x1,0x2), \
  WAVE_BYTE(lvl,0x2,0x2), \
  WAVE_BYTE(lvl,0x2,0x2), \
  WAVE_BYTE(lvl,0x3,0x3), \
  WAVE_BYTE(lvl,0x3,0x3), \
  WAVE_BYTE(lvl,0x3,0x3), }
#define PER_WAVE_ANL(lvl) { \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x1,0x1), \
  WAVE_BYTE(lvl,0x1,0x2), \
  WAVE_BYTE(lvl,0x2,0x2), \
  WAVE_BYTE(lvl,0x2,0x2), \
  WAVE_BYTE(lvl,0x3,0x3), \
  WAVE_BYTE(lvl,0x3,0x3), \
  WAVE_BYTE(lvl,0x3,0x3), \
  WAVE_BYTE(lvl,0x3,0x3), \
  WAVE_BYTE(lvl,0x3,0x4), \
  WAVE_BYTE(lvl,0x4,0x4), \
  WAVE_BYTE(lvl,0x4,0x4), \
  WAVE_BYTE(lvl,0x4,0x4), \
  WAVE_BYTE(lvl,0x4,0x4), \
  WAVE_BYTE(lvl,0x4,0x4), }
#define MAIN_WAVE_DIG(lvl) { \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), }
#define PER_WAVE_DIG(lvl) { \
  WAVE_BYTE(lvl,0xF,0xF), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), \
  WAVE_BYTE(lvl,0x0,0x0), }

static const BYTE s_wave[2][LEVEL_COUNT][0x10] = {
  {
    MAIN_WAVE_ANL(0x0), MAIN_WAVE_ANL(0x1), MAIN_WAVE_ANL(0x2), MAIN_WAVE_ANL(0x3),
    MAIN_WAVE_ANL(0x4), MAIN_WAVE_ANL(0x5), MAIN_WAVE_ANL(0x6), MAIN_WAVE_ANL(0x7),
    MAIN_WAVE_ANL(0x8), MAIN_WAVE_ANL(0x9), MAIN_WAVE_ANL(0xA), MAIN_WAVE_ANL(0xB),
    MAIN_WAVE_ANL(0xC), MAIN_WAVE_ANL(0xD), MAIN_WAVE_ANL(0xE), MAIN_WAVE_ANL(0xF),
  },
  {
    MAIN_WAVE_DIG(0x0), MAIN_WAVE_DIG(0x1), MAIN_WAVE_DIG(0x2), MAIN_WAVE_DIG(0x3),
    MAIN_WAVE_DIG(0x4), MAIN_WAVE_DIG(0x5), MAIN_WAVE_DIG(0x6), MAIN_WAVE_DIG(0x7),
    MAIN_WAVE_DIG(0x8), MAIN_WAVE_DIG(0x9), MAIN_WAVE_DIG(0xA), MAIN_WAVE_DIG(0xB),
    MAIN_WAVE_DIG(0xC), MAIN_WAVE_DIG(0xD), MAIN_WAVE_DIG(0xE), MAIN_WAVE_DIG(0xF),
  },
};
static const BYTE s_periodic[2][LEVEL_COUNT][0x10] = {
  {
    PER_WAVE_ANL(0x0), PER_WAVE_ANL(0x1), PER_WAVE_ANL(0x2), PER_WAVE_ANL(0x3),
    PER_WAVE_ANL(0x4), PER_WAVE_ANL(0x5), PER_WAVE_ANL(0x6), PER_WAVE_ANL(0x7),
    PER_WAVE_ANL(0x8), PER_WAVE_ANL(0x9), PER_WAVE_ANL(0xA), PER_WAVE_ANL(0xB),
    PER_WAVE_ANL(0xC), PER_WAVE_ANL(0xD), PER_WAVE_ANL(0xE), PER_WAVE_ANL(0xF),
  },
  {
    PER_WAVE_DIG(0x0), PER_WAVE_DIG(0x1), PER_WAVE_DIG(0x2), PER_WAVE_DIG(0x3),
    PER_WAVE_DIG(0x4), PER_WAVE_DIG(0x5), PER_WAVE_DIG(0x6), PER_WAVE_DIG(0x7),
    PER_WAVE_DIG(0x8), PER_WAVE_DIG(0x9), PER_WAVE_DIG(0xA), PER_WAVE_DIG(0xB),
    PER_WAVE_DIG(0xC), PER_WAVE_DIG(0xD), PER_WAVE_DIG(0xE), PER_WAVE_DIG(0xF),
  },
};

void sn76489_init(void)
{
  int i;
  memset(&s_sn76489, 0, sizeof(s_sn76489));
  s_sn76489.stereo = 0xFF;

  sound_set_channel(0x8F);
  sound_set_noise(0x19);
  sn76489_set_volume(vgm_volume_level());
  for(i = 0; i < 4; i++) {
    sound_set_volume(i, 0);
    vis_set_volume(i, 0, 0);
  }
}

#define VOL_MUL(v) (v)
static const WORD s_vol_tbl[0x10] = {
  VOL_MUL(32767), VOL_MUL(26028), VOL_MUL(20675), VOL_MUL(16422),
  VOL_MUL(13045), VOL_MUL(10362), VOL_MUL( 8231), VOL_MUL( 6568),
  VOL_MUL( 5193), VOL_MUL( 4125), VOL_MUL( 3277), VOL_MUL( 2603),
  VOL_MUL( 2067), VOL_MUL( 1642), VOL_MUL( 1304), VOL_MUL(    0),
};
static WORD prv_freq_from_reg(WORD reg, int scaling)
{
  /* Sorta gross, but w/e */
  DWORD freq;
  switch(scaling) {
    case -1:
      break;
    case 0:
      reg *= 16;
      break;
    default:
      break;
  }
  if(reg == 0) reg = 1;
  freq = (DWORD)111861 / reg;
  freq = ((DWORD)96000 / freq);
  if(freq > 2048) freq = 2048;
  return 2048 - freq;
}

static BYTE prv_reg_to_volume(int ch, BYTE reg)
{
  DWORD vol = s_vol_tbl[reg];
  if(ch == 3 && s_sn76489.regs[6] & 4)
    return vol * (DWORD)(vgm_volume_level() >> 4) / (DWORD)32768;
  return vol * 0x10 / 32768;
}

static void prv_set_volume(int ch, BYTE reg, BYTE stereo)
{
  reg = 0xF - reg;
  vis_set_volume(ch, stereo & 0x10 ? reg : 0, stereo & 0x01 ? reg : 0);
}

void sn76489_set_volume(WORD vol)
{
  int use_dig = 0;
  s_sn76489.vol = vol * (LEVEL_COUNT-1) / VGM_VOLUME_MAX;
  if(s_sn76489.vol < 8)
    use_dig = 1;
  sound_set_wave(0, (BYTE far*)s_wave[use_dig][s_sn76489.vol]);
  sound_set_wave(1, (BYTE far*)s_wave[use_dig][s_sn76489.vol]);
  sound_set_wave(2, (BYTE far*)s_wave[use_dig][s_sn76489.vol]);
  sound_set_wave(3, (BYTE far*)s_periodic[use_dig][s_sn76489.vol]);
}

static void prv_update_reg(WORD reg)
{
  int ch = s_sn76489.reg_latch >> 1;
  WORD out;
  if(s_sn76489.reg_latch & 1) { /* Volume */
    prv_set_volume(ch, reg, s_sn76489.stereo >> ch);
    reg = prv_reg_to_volume(ch, reg);
    out = 0;
    if(s_sn76489.stereo & (0x01 << ch)) {
      out |= (reg << 0);
    }
    if(s_sn76489.stereo & (0x10 << ch)) {
      out |= (reg << 4);
    }
    sound_set_volume(ch, out);
  }else{ /* tone */
    if(ch == 3) { /* noise */
      if(reg & 4) {
        sound_set_channel(0x8F);
      }else{
        sound_set_channel(0x0F);
      }
      switch(reg & 3) {
        case 0: reg = 0x10; break;
        case 1: reg = 0x20; break;
        case 2: reg = 0x40; break;
        case 3: reg = s_sn76489.regs[0x4]; break;
      }
      sound_set_pitch(ch, prv_freq_from_reg(reg,s_sn76489.regs[6] & 4));
    }else{
      if(ch == 2 && ((s_sn76489.regs[6] & 3) == 3))
        sound_set_pitch(3, prv_freq_from_reg(reg,s_sn76489.regs[6] & 4));
      sound_set_pitch(ch, prv_freq_from_reg(reg,-1));
    }
  }
}

void sn76489_write(BYTE data)
{
  if(data & (1 << 7)) { /* LATCH/DATA */
    s_sn76489.reg_latch = (data >> 4) & 7;
    s_sn76489.regs[s_sn76489.reg_latch] &= ~0xF;
    s_sn76489.regs[s_sn76489.reg_latch] |= data & 0xF;
  }else{ /* DATA */
    data &= 0x3F;
    if(s_sn76489.reg_latch & 1 || s_sn76489.reg_latch == 6) {
      s_sn76489.regs[s_sn76489.reg_latch] &= ~0xF;
      s_sn76489.regs[s_sn76489.reg_latch] |= data & 0xF;
    }else{
      s_sn76489.regs[s_sn76489.reg_latch] &= ~0x3F0;
      s_sn76489.regs[s_sn76489.reg_latch] |= (data & 0x3F) << 4;
    }
  }
  prv_update_reg(s_sn76489.regs[s_sn76489.reg_latch]);
}

void sn76489_ggstereo(BYTE data)
{
  s_sn76489.stereo = data;
}
