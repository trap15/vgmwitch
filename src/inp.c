#include "top.h"

#include "gfx.h"
#include "inp.h"

#define AUTOREP_DELAY_MAX 30

WORD g_inp, g_inp_down, g_inp_up;
WORD g_inp_rep, g_inp_rep_down, g_inp_rep_up;

WORD g_inp_buf[AUTOREP_DELAY_MAX];

int g_repeat_rate, g_repeat_delay;

void inp_set_repeat_rate(int rate)
{
  g_repeat_rate = rate;
}
void inp_set_repeat_delay(int delay)
{
  g_repeat_delay = delay;
}

void inp_init(void)
{
  inp_set_repeat_rate(2);
  inp_set_repeat_delay(12);

  g_inp = 0;
  g_inp_down = 0;
  g_inp_up = 0;
  g_inp_rep = 0;
  g_inp_rep_down = 0;
  g_inp_rep_up = 0;
  memset(g_inp_buf, 0, sizeof(g_inp_buf));
  inp_update();
}
void inp_finish(void)
{
}

void inp_update(void)
{
  WORD new, mask, i;
  new = key_press_check();
  g_inp_up = (new ^ g_inp) & g_inp;
  g_inp_down = (new ^ g_inp) & new;
  g_inp = new;

  memcpy(g_inp_buf, g_inp_buf+1, sizeof(g_inp_buf) - 2);
  g_inp_buf[AUTOREP_DELAY_MAX-1] = g_inp;
  new = g_inp;
  if((g_frame % g_repeat_rate) == 0) {
    mask = ~0;
    for(i = 0; i < g_repeat_delay; i++) {
      mask &= g_inp_buf[AUTOREP_DELAY_MAX-1-i];
    }
    new &= ~mask;
  }
  g_inp_rep_up = (new ^ g_inp_rep) & g_inp_rep;
  g_inp_rep_down = (new ^ g_inp_rep) & new;
  g_inp_rep = new;
}

void inp_rep_clear(void)
{
  g_inp_buf[AUTOREP_DELAY_MAX-1] = 0;
}
