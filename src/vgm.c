#include "top.h"

#include "vgm.h"
#include "ui/gd3.h"
#if DEFLATE_SUPPORT
#include "zlib/puff.h"
#endif
#include "chip/sn76489.h"

#define VGMFLAG_PLAYING (1 << 0)
#define VGMFLAG_DONE    (1 << 1)
#define VGMFLAG_PAUSED  (1 << 2)

#define DEFAULT_MAX_LOOPS 2

#define VGM_BUFFER_SIZE (0x10000 - 0x1F20)

#if DEFLATE_SUPPORT
static PuffState s_puffstt;
#endif

typedef struct {
  WORD flag;
  intvector_t saved_irqh;

  /* Implementation deetz */
  DWORD fsize;
  SWORD wait;
  WORD vol_level;

  /* Compression */
  DWORD buffer_end, buffer_pos;

  /* File/ptr */
  BYTE far *file;
  BYTE is_vgz;
  WORD ptr_seg;
  WORD ptr_off;
  WORD vgm_data_offset;
  DWORD idx;
  DWORD samples;

  /* Timestamps */
  TimeStamp time_intro, time_loop, time_full;

  /* Looping */
  DWORD loop_start_left;
  DWORD loop_samples;
  DWORD loop_idx;

  WORD loop_max;
  WORD loop_count;
} VGMState;

static VGMState g_vgm;

/* Improve accuracy on sub-frame waits */
#define ACCURACY_MULTIPLIER 1

static const BYTE s_timer_ticks[] = {
  200/ACCURACY_MULTIPLIER, /* NTSC */
  240/ACCURACY_MULTIPLIER, /* PAL */
};

#define WAIT_ADVANCE (44100/(60*ACCURACY_MULTIPLIER))

BYTE s_vgm_buffer[VGM_BUFFER_SIZE];

static void prv_next_vgm_buffer(void)
{
  WORD seg, off;
  DWORD destlen;
  g_vgm.buffer_pos = g_vgm.buffer_end;
  if(g_vgm.is_vgz) {
    destlen = VGM_BUFFER_SIZE;
#if DEFLATE_SUPPORT
#if STREAM_SUPPORT
    puff(&s_puffstt, s_vgm_buffer, &destlen);
    if(s_puffstt.puff_failed) {
      /* TODO: Oh god we're all dead */
      destlen = VGM_BUFFER_SIZE;
    }
#else
    g_vgm.buffer_pos = 0;
    g_vgm.buffer_end = VGM_BUFFER_SIZE;
    return;
#endif
#endif
  }else{
    /* Tricky hax to make big VGMs work */
    seg = g_vgm.ptr_seg + ((g_vgm.buffer_pos >> 12) << 8);
    off = g_vgm.ptr_off + (g_vgm.buffer_pos & 0xFFF);
    destlen = g_vgm.fsize - g_vgm.buffer_pos;
    if(destlen > VGM_BUFFER_SIZE)
      destlen = VGM_BUFFER_SIZE;
    memcpy(s_vgm_buffer, (BYTE far*)MK_DWORD(seg, off), destlen);
  }
  g_vgm.buffer_end += destlen;
}

static void prv_done(void)
{
  g_vgm.flag &= ~VGMFLAG_PLAYING;
  g_vgm.flag |=  VGMFLAG_DONE;
}

static BYTE prv_r8(void)
{
  SDWORD idx;
  DWORD last;
  if(g_vgm.loop_start_left)
    g_vgm.loop_start_left--;
  idx = g_vgm.idx + g_vgm.vgm_data_offset;
  while(idx >= g_vgm.buffer_end) {
    last = g_vgm.buffer_end;
    prv_next_vgm_buffer();
    if(g_vgm.buffer_end == last) {
      prv_done();
      return 0x66;
    }
  }
  idx -= g_vgm.buffer_pos;
  g_vgm.idx++;
  return s_vgm_buffer[idx];
}

void far vgm_update(void)
{
  BYTE c;
  WORD wait;
  if(!(g_vgm.flag & VGMFLAG_PLAYING))
    return;

  while(g_vgm.wait <= 0) {
    wait = 0;
    c = prv_r8();
    if(c == 0x50) { /* Write SN76489 */
      c = prv_r8();
      sn76489_write(c);
    }else if(c == 0x4F) { /* Write GameGear stereo */
      c = prv_r8();
      sn76489_ggstereo(c);
    }else if(c == 0x61) { /* Wait arbitrary samples */
      wait  = prv_r8();
      wait |= prv_r8() << 8;
    }else if(c == 0x62) { /* Wait 1 NTSC frame */
      wait = 735; /* 1/60th second */
    }else if(c == 0x63) { /* Wait 1 PAL frame */
      wait = 882; /* 1/50th second */
    }else if((c & 0xF0) == 0x70) {
      wait = 1 + (c & 0xF);
    }else if(c == 0x66) { /* End of sound data */
      prv_done();
    }
    g_vgm.wait += wait;
    if(g_vgm.loop_samples && g_vgm.loop_start_left == 0) {
      g_vgm.samples += wait;
      if(g_vgm.samples >= g_vgm.loop_samples) {
        g_vgm.idx = g_vgm.loop_idx;
        g_vgm.buffer_pos = 0; /* Re-start seeking */
        g_vgm.buffer_end = 0; /* Re-start seeking */
        g_vgm.samples = 0;
        g_vgm.loop_count++;
        if(g_vgm.loop_max && g_vgm.loop_count == g_vgm.loop_max) {
          prv_done();
        }
      }
    }

    if(!(g_vgm.flag & VGMFLAG_PLAYING))
      return;
  }
  g_vgm.wait -= WAIT_ADVANCE;
}

void vgm_set_loop_count(int count)
{
  g_vgm.loop_max = count;
}
int vgm_loop_count(void)
{
  return g_vgm.loop_max;
}

int vgm_is_over(void)
{
  return (g_vgm.flag & VGMFLAG_DONE) == VGMFLAG_DONE;
}

int vgm_is_playing(void)
{
  return (g_vgm.flag & VGMFLAG_PLAYING) == VGMFLAG_PLAYING &&
         !vgm_is_over();
}

void vgm_init(void)
{
  intvector_t irqh;

  sound_init();
  sound_mute();
  memset(&g_vgm, 0, sizeof(g_vgm));
  g_vgm.loop_max = DEFAULT_MAX_LOOPS;
  g_vgm.flag = VGMFLAG_DONE;
  g_vgm.vol_level = VGM_VOLUME_MAX;

  irqh.callback = (void near*)FP_OFF(vgm_update);
  irqh.cs = _CS;
  irqh.ds = _DS;

  sys_interrupt_set_hook(SYS_INT_HBLANK_COUNTUP, &irqh, &g_vgm.saved_irqh);
}

static void prv_samples_to_stamp(DWORD samples, TimeStamp *stmp)
{
  DWORD wrk;
  wrk = samples * 151 / 44100;
  stmp->dframe = wrk % 151;
  wrk /= 151;
  samples = wrk;
  stmp->sec = wrk % 60;
  wrk /= 60;
  stmp->min = wrk;

  if(samples == 0) {
    stmp->valid = 0;
  }else{
    stmp->valid = 1;
  }
}

void far *farptr_regulate(void far *ptr)
{
  DWORD fp = (DWORD)ptr;
  WORD l, h;
  l = fp & 0xFFFF;
  h = fp >> 16;
  h += l >> 4;
  l &= 0xF;
  fp = ((DWORD)h << 16) | l;
  return (void far*)fp;
}

int vgm_load(BYTE far *file, DWORD size)
{
  BYTE ticks;
  DWORD samples;

  file = farptr_regulate(file);

  sound_mute();

  g_vgm.flag = VGMFLAG_DONE;
  g_vgm.samples = 0;
  g_vgm.wait = 0;
  g_vgm.loop_count = 0;

  g_vgm.file = file;
  g_vgm.fsize = size;
  if(strncmp(file, "\x1F\x8B\x08", 3) != 0) { /* Check for DEFLATE header */
    g_vgm.is_vgz = 0;
  }else{
    g_vgm.is_vgz = 1;
#if DEFLATE_SUPPORT
    g_vgm.file += 10;
    if(file[3] & 8) {
      g_vgm.file += strlen(g_vgm.file) + 1;
    }
    g_vgm.fsize -= 10 + 8;
    g_vgm.fsize = 65535; // TODO: HAX

    puff_init(&s_puffstt,
              g_vgm.file, g_vgm.fsize);
#endif
#if !STREAM_SUPPORT
    samples = VGM_BUFFER_SIZE;
    puff(&s_puffstt, s_vgm_buffer, &samples);
    if(s_puffstt.puff_failed) {
      g_vgm.file = NULL;
      return s_puffstt.puff_failed;
    }
#endif
  }
  g_vgm.buffer_pos = 0;
  g_vgm.buffer_end = 0;
  g_vgm.ptr_seg = FP_SEG(g_vgm.file);
  g_vgm.ptr_off = FP_OFF(g_vgm.file);
  g_vgm.vgm_data_offset = 0;

  prv_next_vgm_buffer();

#if !DEFLATE_SUPPORT || !STREAM_SUPPORT
  /* TODO: I'm bad */
  gd3_load(s_vgm_buffer);
#endif

  g_vgm.vgm_data_offset = 0x40;
  sn76489_init();

  if(s_vgm_buffer[0x24] == 60) {
    ticks = s_timer_ticks[0];
  }else if(s_vgm_buffer[0x24] == 50) {
    ticks = s_timer_ticks[1];
  }else{
    ticks = s_timer_ticks[0];
  }
  g_vgm.fsize = *(DWORD far*)(&s_vgm_buffer[0x04]);

  g_vgm.loop_samples = *(DWORD far*)(&s_vgm_buffer[0x20]);
  g_vgm.loop_start_left = *(DWORD far*)(&s_vgm_buffer[0x1C]) + 0x1C - g_vgm.vgm_data_offset;
  g_vgm.loop_idx = g_vgm.loop_start_left;
  g_vgm.idx = 0;

  samples = *(DWORD far*)(&s_vgm_buffer[0x18]);
  samples -= g_vgm.loop_samples;
  prv_samples_to_stamp(samples, &g_vgm.time_intro);
  prv_samples_to_stamp(g_vgm.loop_samples, &g_vgm.time_loop);
  prv_samples_to_stamp(samples + g_vgm.loop_samples * g_vgm.loop_max, &g_vgm.time_full);

  timer_enable(TIMER_HBLANK, 1, ticks);
  return 0;
}

void vgm_start(void)
{
  if(g_vgm.file == NULL)
    return;
  g_vgm.flag = VGMFLAG_PLAYING;
  sound_unmute();
}
void vgm_pause(void)
{
  if(g_vgm.flag & VGMFLAG_PAUSED) {
    vgm_start();
  }else{
    g_vgm.flag &= ~VGMFLAG_PLAYING;
    g_vgm.flag |= VGMFLAG_PAUSED;
    sound_mute();
  }
}
void vgm_stop(void)
{
  vgm_pause();
  g_vgm.file = NULL;
  g_vgm.flag = VGMFLAG_DONE;
  timer_disable(TIMER_HBLANK);
  sound_mute();
}

TimeStamp vgm_time_intro(void)
{
  return g_vgm.time_intro;
}

TimeStamp vgm_time_loop(void)
{
  return g_vgm.time_loop;
}

TimeStamp vgm_time_full(void)
{
  return g_vgm.time_full;
}


void vgm_volume_set(SWORD level)
{
  if(level < 0) level = 0;
  if(level > VGM_VOLUME_MAX) level = VGM_VOLUME_MAX;
  g_vgm.vol_level = level;
  sn76489_set_volume(level);
}
WORD vgm_volume_level(void)
{
  return g_vgm.vol_level;
}
void vgm_volume_down(void)
{
  vgm_volume_set(vgm_volume_level() - VGM_VOLUME_STEP);
}
void vgm_volume_up(void)
{
  vgm_volume_set(vgm_volume_level() + VGM_VOLUME_STEP);
}

int vgm_is_vgz(void)
{
  return g_vgm.is_vgz;
}
