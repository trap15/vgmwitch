#ifndef GFX_H_
#define GFX_H_

#define SCREENTEXT SCREEN2

#define HW_SPR_COUNT 0x80
#define SW_SPR_COUNT 0x80

#define BGPALOFF (8)

#define BGPAL_TEXT     (0x4)

typedef struct Sprite {
  WORD attr;
  WORD tile;
  WORD x, y;
  /* .b0   = enable
   * .b1   = invisible
   */
  WORD extra;
} Sprite;

extern int g_frame;

void gx_init(void);
void gx_finish(void);

void gx_frame(void);

Sprite *gx_spr_alloc(void);
int gx_spr_alloc_count(int count, Sprite *sprs[]);

void gx_set_bgcolor(BYTE col);

#endif
