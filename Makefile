.SUFFIXES: .fx .bin .obj .a86 .c

CRT0=crt0asc2.obj

OUTNAME=vgmwitch
FXNAME=vgmwitch
OBJECTS=\
	src/main.obj src/gfx.obj src/inp.obj \
	src/vgm.obj \
		src/chip/sn76489.obj \
	src/ui/ui.obj \
		src/ui/vis.obj \
		src/ui/gd3.obj \
	src/zlib/puff.obj \

CP=cp -p
RM=rm -f

CFLAGS=-Isrc/ -I./ -B
DEFINES=
ASFLAGS=

CLEANS=$(OBJECTS) $(OUTNAME).bin $(OUTNAME).map files

all: $(FXNAME).fx $(STAGEFX)

$(FXNAME).fx: $(OUTNAME).bin
	mkfent $*.cf

$(OUTNAME).bin: $(OBJECTS)
	echo "$(OBJECTS)" > files
	lcc86 -a$(CRT0) -k-M -o $*.bin @files

%.obj: %.a86
	r86 $(ASFLAGS) -o $@ $<

%.obj: %.c
	lcc86 $(CFLAGS) -SC $(DEFINES) -o tmp.z86 $< $(CFLAG_SFX)
	cat asm_pfx tmp.z86 > tmp.y86
	r86 $(ASFLAGS) -m $< -o $@ tmp.y86
	rm tmp.z86 tmp.y86

%.a86: %.c
	lcc86 -SC $(CFLAGS) $(DEFINES) -o $@ $<

%.a86: %.p86
	cpp -o $@ $<

clean:
	$(RM) $(CLEANS)
	$(RM) *.fx

fullclean:
	$(RM) $(CLEANS) $(RSRC)
	$(RM) *.fx

distclean:
	$(RM) $(CLEANS) $(RSRC)
