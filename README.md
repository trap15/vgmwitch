# VGM Witch

Application to play SN76489 VGM files on WonderWitch.

## What do I need to run this?

* A WonderSwan/Color/Crystal
* A WonderWitch with serial cable
* VGM or VGZ files

## How do I run this?

* Upload `vgmwitch.fx` to your WonderWitch
* Upload any number of VGM or VGZ files to /rom0
* Run `VGM Witch` from Freya and enjoy the tunes

## Who's responsible for this garbage?

It's me, I'm responsible.

Also I'm using a modified version of Mark Adler's `puff` for gzip decompression (in `src/zlib/puff.[c|h]`).

## Changelog

### v0.3

* Increase file list entry maximum to 128
* Bugfixes for VGZ support
* Better decompression error reporting
* Fix looping

### v0.2

* A bunch of cleanups
* Support for VGZs that decompress to under ~57K
* Playback time display
* Volume control (Y1/Y3)
* Next/prev control (Y2/Y4)
* Some changes to control scheme
* Improved visualizer's output
* Support for looping

### v0.1 - Initial Release/Source Upload

* It's out :V

## TODO

* I want to do streaming VGZ support so that any size of VGZ can work
* Add a configuration menu
* More sound chips?
* Make the GZ header parsing more robust
* Make the VGM header parsing more robust (and support newer revisions)

## Links

* <http://daifukkat.su/> - My website
* <http://bitbucket.org/trap15/vgmwitch/> - This repository

## Greetz

* Ryphecha
* Charles MacDonald
* austere
* \#raidenii - Forever impossible

### Licensing

All contents within this repository (unless otherwise specified) are licensed under the following terms:

The MIT License (MIT)

Copyright (C)2015 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
